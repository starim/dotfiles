#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# append to the ~/.bash_history file when the shell closes rather than overwriting it
shopt -s histappend
# flush bash commands to ~/.bash_history immediately after executing them
# instead waiting until the shell is closed
export PROMPT_COMMAND='history -a'
# ~/.bash_history length (default is 500)
HISTSIZE=10000

export EDITOR=nvim

alias ls='ls --color=auto'
alias cp='cp -i'
alias mv='mv -i'
alias lock='xscreensaver-command --lock'

# default prompt, overriden below
# PS1='[\u@\h \W]\$ '

# make less use lesspipe.sh automatically to preprocess files that aren't plain
# text so they can be viewed meaningfully in less (requires lesspipe be
# installed)
export LESSOPEN="| /usr/bin/lesspipe.sh %s"

# make Gnome show scrollbars instead of hiding them
export GTK_OVERLAY_SCROLLING=0

# activate rbenv
# PATH="$HOME/.rbenv/bin:$PATH"
# eval "$(rbenv init -)"

# tell bundler to install gems for this user only
export GEM_HOME=$(ruby -e 'puts Gem.user_dir')

# add Ruby gem executables to the PATH
PATH="$(ruby -e 'puts Gem.user_dir')/bin:$PATH"

# activate bash completion for git commands
[[ -r /usr/share/git/completion/git-completion.bash ]] && source /usr/share/git/completion/git-completion.bash

# show git info in the terminal prompt when in a git repo
source /usr/share/git/completion/git-prompt.sh
PS1='[\u \W$(__git_ps1 " (%s)")]\$ '

# alias tmux so that tmux knows it can emit Unicode and the terminal will be
# able to understand it
alias tmux="tmux -u"

# enable coloring directories shown by ls
# from https://github.com/seebi/dircolors-solarized
eval `dircolors ~/Projects/dotfiles/.dir_colors`

eval $(ssh-agent)

# disable software flow control in terminal
stty -ixon

# switch bash to vi mode
set -o vi

# used for running Steam as a different user
alias steam-login="sudo -u wine -s"

dotfiles
========

A collection of configuration files.

Use `install-vim-plugins.sh` to register a new vim plugin.
Use `update-vim-plugins.sh` to update already registered vim plugins.

Installation
==

Copy or symlink the following files to these locations:
* `.bashrc` -> `~/.bashrc`
* `.gitconfig` -> `~/.gitconfig`
* `.gitignore-global` -> `~/.gitignore-global`
* `.tmux.conf` -> `~/.tmux.conf`
* `.vimrc` -> `~/.vimrc`
* `.vim` -> `~/.vim`
* `nvim` -> `~/.config/nvim`

Additional Packages for Neovim
--

Deoplete requires the following extra packages be installed:
* msgpack (`python-msgpack` on Arch)
* pynvim (`python-pynvim` on Arch)


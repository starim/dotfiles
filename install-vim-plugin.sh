#!/usr/bin/env bash

set -euo pipefail

name="$1"
clone_url="$2"
branch="$3"

git subtree add --prefix "nvim/pack/$name/start/$name" "$clone_url" "$branch" --squash

update_submodule_line="git subtree pull --prefix 'nvim/pack/$name/start/$name' '$clone_url' '$branch' --squash"
echo "$update_submodule_line" >> "$(dirname -- "$0";)"/update-vim-plugins.sh

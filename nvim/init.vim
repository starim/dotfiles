" turn off folding by indent for this file
set modelines=1

" General settings section.
syntax enable    " syntax highlighting
set encoding=utf-8
set fileencoding=utf-8
                 " use UTF-8 encoding by default
set number       " display line numbers
set scrolloff=8  " the number of rows that will always be visible above and
                 " below the cursor
set titlestring=%f title
                 " terminal vim only--sets the terminal's title to the
                 " name of the file being edited
set ttyfast      " faster redrawing when scrolling
set visualbell   " flash the screen instead of using audio alerts
set showcmd      " show previously entered command in the bottom right
set hlsearch     " highlight matches when searching
set ignorecase   " set searches to be case insensitive...
set smartcase    " ...unless the search explicitly uses uppercase characters
set incsearch    " dynamically show matches as search terms are typed in
set autoindent   " when creating a new line, copy the previous lines'
                 " indentation
set cursorline   " highlight the current line the cursor is on
set lazyredraw   " redraw the screen less often
set showmatch    " highlight bracket or parenthesis that matches the one the
                 " cursor is on
set foldenable   " enable folding
set foldlevelstart=10
                 " automatically close folds deeper than 4 levels
set foldnestmax=10
                 " stop nesting folds at 10 levels deep
set foldmethod=indent
                 " fold based on indent level
set hidden       " allow swapping buffers without first saving changes
set wildmenu     " see options when tab-completion is ambiguous
set wildmode=list:longest
                 " when tab-completion is ambiguous, complete up to the
                 " point of ambiguity
set backspace=indent,eol,start
                 " make backspace work across lines and automatic indenting
set pastetoggle=<F2>
                 " temporarily disable auto-indenting by pressing F2
nnoremap <SPACE> <Nop>
let mapleader = " "
                 " set the leader character to spacebar replacing the default
                 " hard-to-type \
                 " note that to do this spacebar has to be unmapped from
                 " <right> first
set history=100  " extend vim's history from 20 commands to 100
set clipboard=unnamedplus " yank to the system clipboard by default
runtime macros/matchit.vim
                 " extend the % key to switch between if/else or begin/end
                 " keywords in source code in addition to its default
                 " behavior of switching between matching brackets
                 " NOTE: this requires the matchit macro be installed
" Enable file type detection and type-specific indentation.
filetype plugin on
filetype indent on


" Search behavior section. These settings remap the standard search
" commands to fancier commands that force the search results to the center
" of the screen, for the most comfortable searching experience.
:nnoremap n nzz
:nnoremap N Nzz
:nnoremap * *zz
:nnoremap # #zz
:nnoremap g* g*zz
:nnoremap g# g#zz

" move vertically by visual line
nnoremap j gj
nnoremap k gk

autocmd Filetype gitcommit setlocal spell textwidth=72

" Tab behavior section.
"set expandtab    " convert tabs to spaces, so Kevin won't rage
set noexpandtab  " don't convert tabs to spaces (personal preference)
set tabstop=4    " tabs are represented in the Windows style, with a width
                 " of 4 spaces
set shiftwidth=4 " sets the number of spaces the << and >> operators indent;
                 " this should match the tabstop setting


" Force true color mode to support the Solarized theme.
set termguicolors

" This section loads my Solarized theme.
set background=dark
colorscheme NeoSolarized


" This section sets backup (*~) and swap (*.swp) files to be stored in
" one central location instead of cluttering up the working directory.
" Each setting is a comma separated list of locations to store files;
" vim will store the files in the first directory in the list that both
" exists and is writable.
set backup
set writebackup
set backupdir=~/.vim-backup,/tmp
set directory=~/.vim-backup,/tmp

" Suggested by the vim-airline wiki to fix vim-airline not showing up until a
" split is created.
set laststatus=2

" Automatically displays all buffers in airline when there's only one tab
" open.
let g:airline#extensions#tabline#enabled = 1

" Use the solarized theme for airline.
"let g:airline_theme = 'solarized'
let g:airline_theme = 'murmur'

" display status messages from ALE in vim-airline
let g:airline#extensions#ale#enabled = 1

let g:deoplete#enable_at_startup = 1

" Use ALE as a completion source for Deoplete for all file types.
call deoplete#custom#option('sources', {
\ '_': ['ale'],
\})

" This setting must be turned on before ALE is loaded.
" Enable completion in ALE where available.
" Turn this off if ALE should be used as a completion source for other
" completion plugins. Currently I'm using it as a completion source for
" deoplete.
let g:ale_completion_enabled = 0

" use rust-analyzer as a linter in ALE
let g:ale_linters = {'rust': ['analyzer']}

" Entable this to format files on save. Requires fixers be set up for the file
" type. See the documetation here: https://github.com/dmerejkowsky/vim-ale#user-content-2ii-fixing
let g:ale_fix_on_save = 1

" shortcuts
map <leader>g :ALEGoToDefinition<CR>
map <leader>r :ALEFindReferences<CR>


#!/usr/bin/env bash

set -euo pipefail

git subtree pull --prefix 'nvim/pack/deoplete/start/deoplete' 'https://github.com/Shougo/deoplete.nvim.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/ale/start/ale' 'https://github.com/dense-analysis/ale.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/vim-airline/start/vim-airline' 'https://github.com/vim-airline/vim-airline.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/vim-airline-themes/start/vim-airline-themes' 'https://github.com/vim-airline/vim-airline-themes.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/vim-gitgutter/start/vim-gitgutter' 'https://github.com/airblade/vim-gitgutter.git' 'main' --squash
git subtree pull --prefix 'nvim/pack/vim-bundler/start/vim-bundler' 'https://github.com/tpope/vim-bundler.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/vim-commentary/start/vim-commentary' 'https://github.com/tpope/vim-commentary.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/vim-gutentags/start/vim-gutentags' 'https://github.com/ludovicchabant/vim-gutentags.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/vim-rails/start/vim-rails' 'https://github.com/tpope/vim-rails.git' 'master' --squash
git subtree pull --prefix 'nvim/pack/vim-surround/start/vim-surround' 'https://github.com/tpope/vim-surround.git' 'master' --squash
